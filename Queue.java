import java.util.ArrayList;

public class Queue extends Thread {
	ArrayList<Customer> customers = new ArrayList<Customer>();
	int currentWaitingTime = 0;
	int totalWaitingTime = 0;
	int servicedClientsTime = 0;
	int maxNoCustomers = 0;
	int servicedClientsNumber = 0;
	int noCustomers = 0;
	String queueName;
	public boolean stop = false;
	long startTime;
	
	public Queue(long value, int index){
		startTime = value;
		queueName = "Queue " + index;
	}
	public void stopThread(){
		stop = true;
	}
	public void addCustomer(Customer customer){
		customers.add(customer);
		currentWaitingTime+= customer.serviceTime;
		totalWaitingTime+= customer.serviceTime;
		noCustomers++;
	}
	public void removeCustomer(Customer customer){
		int indexOfCustomer = customers.indexOf(customer);
		currentWaitingTime-=customers.get(indexOfCustomer).serviceTime;
		totalWaitingTime-=customers.get(indexOfCustomer).serviceTime;
		noCustomers--;
		customers.remove(indexOfCustomer);
	}
	public void customerFinishesService(){
		currentWaitingTime-=customers.get(0).serviceTime;
		servicedClientsTime+=customers.get(0).serviceTime;
		customers.remove(0);
		noCustomers--;
		servicedClientsNumber++;
	}
	public void printQueue(){
		for(Customer customer:customers){
			System.out.println("Customer " + customer.serviceTime);
		}
	}
	public int averageWaitingTime(){
		if(servicedClientsNumber!=0){
			return servicedClientsTime/servicedClientsNumber;
		}
		else return -1;
	}
	public void run(){
		while(!stop){
			if(stop){
				return;
			}
			synchronized (this){
				if(noCustomers>0){
					try {
						wait(customers.get(0).serviceTime);
					}catch (InterruptedException e) {
						break;
					}
				}else{
					try {
						wait();
					} catch (InterruptedException e) {
						break;
					}
				}
			}
			if(startTime + customers.get(0).serviceTime + servicedClientsTime < System.currentTimeMillis()){
				customerFinishesService();
			}
			System.out.println(queueName + ": " + noCustomers + " customers;");
		}
	}
}