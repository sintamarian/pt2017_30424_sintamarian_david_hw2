import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Shop extends TimerTask  {
	static int minInterval = 10, maxInterval = 1000;
	static int minServiceTime = 3000, maxServiceTime = 4000;
	Timer timer = new Timer();
	static long startTime = System.currentTimeMillis();
	static long simulationDuration = 10000;
	static ArrayList<Queue> queues = new ArrayList<Queue>();
	static int noQueues = 3;
	static int maxNoCustomers = 0;
	static int init = 0;
	static double peakHour;
	
	public Shop(int value){
		init = value;
	}


	public void run() {
        int delay = minInterval + (int)(Math.random() * ((maxInterval - minInterval) + 1));
        int serviceTime = minServiceTime + (int)(Math.random()*((maxServiceTime-minServiceTime) + 1));
        int index;
        if(System.currentTimeMillis() - startTime > simulationDuration && allQueuesEmpty()){
        	for(Queue queue: queues){
    	 		queue.stopThread();
     		}
        	System.out.println("Peak hour: " + peakHour/1000);
        	double average = 0;
        	double servicedClients = 0;
        	double servicedTime = 0;
        	for(Queue queue:queues){
        		average = queue.totalWaitingTime;
        		servicedClients += queue.servicedClientsNumber;
        		servicedTime += queue.servicedClientsTime;
        	}
        	System.out.println("Average waiting time: " + (average/noQueues)/1000);
        	System.out.println("Average service time: " + (servicedTime/servicedClients)/1000);
        	return;
        }
        if(System.currentTimeMillis() - startTime < simulationDuration){
	        Customer customer = new Customer(serviceTime);
	        index = getShortestQueue();
	        queues.get(index).addCustomer(customer);
	        if(maxNoCustomers < getAllCustomers()){
	        	maxNoCustomers = getAllCustomers();
	        	peakHour = System.currentTimeMillis() - startTime;
	        }
	        synchronized (queues.get(index)){
	        	queues.get(index).notify();
	        }
        }
        timer.schedule(new Shop(1), delay);
    }
    
    public static int getAllCustomers(){
    	int noCustomers = 0;
    	for(Queue queue:queues){
    		noCustomers+= queue.noCustomers;
    	}
    	return noCustomers;
    }
    
    public static boolean allQueuesEmpty(){
	    for(Queue queue:queues){
    		if(queue.customers.size() > 0){
	    		return false;
	    	}
	    }
    	return true;
    }
    
    public static  int getShortestQueue(){
    	int shortestQueue;
    	shortestQueue = 0;
    	for(int i = 0; i<queues.size() ; ++i){
    		if(queues.get(i).noCustomers < queues.get(shortestQueue).noCustomers){
    			shortestQueue = i;
    		}
    	}
    	return shortestQueue;
    }
    
    static public void start(){
    	for(int i = 0; i< noQueues; ++i){
	 		queues.add(new Queue(startTime, i+1));
	 		queues.get(i).start();
 		}
 		Shop shop = new Shop(1);
 		shop.run();
    }
    private static PrintStream standardOut;
    public static void main(String[] args) {
    	JTextArea textArea = new JTextArea(50, 10);
        PrintStream printStream = new PrintStream(new CustomOutputStream(textArea));
    	standardOut = System.out;
    	System.setOut(printStream);
        System.setErr(printStream);
    	JFrame frame = new JFrame();
    	JButton buttonStart = new JButton("Start");
    	JTextArea minimumArrivingTime = new JTextArea(50, 10);
    	JTextArea maximumArrivingTime = new JTextArea(50, 10);
    	JTextArea minimumServiceTime = new JTextArea(50, 10);
    	JTextArea maximumServiceTime = new JTextArea(50, 10);
    	JTextArea numberOfQueues = new JTextArea(50, 10);
    	JTextArea simTime = new JTextArea(50, 10);
    	JPanel panel = new JPanel();
    	frame.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(10, 10, 10, 10);
        constraints.anchor = GridBagConstraints.WEST;
        frame.add(buttonStart, constraints);
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.insets = new Insets(10, 10, 10, 10);
        constraints.anchor = GridBagConstraints.WEST;
        frame.add(minimumArrivingTime, constraints);
        constraints.gridy = 2;
        frame.add(maximumArrivingTime, constraints);
        constraints.gridy = 3;
        frame.add(minimumServiceTime, constraints);
        constraints.gridy = 4;
        frame.add(maximumServiceTime, constraints);
        constraints.gridy = 5;
        frame.add(numberOfQueues, constraints);
        constraints.gridy = 6;
        frame.add(simTime, constraints);
        constraints.gridx = 3;
        constraints.gridy = 0;
        constraints.gridheight = 5;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        frame.add(new JScrollPane(textArea), constraints);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(480, 320);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        buttonStart.addActionListener( new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {

                if(init == 0){
//	            	noQueues = Integer.parseInt(numberOfQueues.getText());
//	            	simulationDuration = Long.parseLong(simTime.getText())*1000;
//	            	minServiceTime = Integer.parseInt(minimumServiceTime.getText())*1000;
//	            	maxServiceTime = Integer.parseInt(maximumServiceTime.getText())*1000;
//	            	minInterval = Integer.parseInt(minimumArrivingTime.getText())*1000;
//	            	maxInterval = Integer.parseInt(maximumArrivingTime.getText())*1000;
//	            	init = 1;
//	            	System.out.println(minInterval);
//	            	System.out.println(maxInterval);
//	            	System.out.println(minServiceTime);
//	            	System.out.println(maxServiceTime);
//	            	System.out.println(simulationDuration);
//	            	System.out.println(noQueues);
                }
            	start();
            }
        });
    }
 }