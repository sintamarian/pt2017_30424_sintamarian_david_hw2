import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class UI {

    private static PrintStream standardOut;

	public void initUi() {
		JTextArea textArea = new JTextArea(50, 10);
        PrintStream printStream = new PrintStream(new CustomOutputStream(textArea));
    	standardOut = System.out;
    	System.setOut(printStream);
        System.setErr(printStream);
    	JFrame frame = new JFrame();
    	JButton buttonStart = new JButton("Start");
    	JTextArea minimumArrivingTime = new JTextArea(50, 10);
    	JTextArea maximumArrivingTime = new JTextArea(50, 10);
    	JTextArea minimumServiceTime = new JTextArea(50, 10);
    	JTextArea maximumServiceTime = new JTextArea(50, 10);
    	JTextArea numberOfQueues = new JTextArea(50, 10);
    	JTextArea simTime = new JTextArea(50, 10);
    	JPanel panel = new JPanel();
    	frame.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(10, 10, 10, 10);
        constraints.anchor = GridBagConstraints.WEST;
        frame.add(buttonStart, constraints);
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.insets = new Insets(10, 10, 10, 10);
        constraints.anchor = GridBagConstraints.WEST;
        frame.add(minimumArrivingTime, constraints);
        constraints.gridy = 2;
        frame.add(maximumArrivingTime, constraints);
        constraints.gridy = 3;
        frame.add(minimumServiceTime, constraints);
        constraints.gridy = 4;
        frame.add(maximumServiceTime, constraints);
        constraints.gridy = 5;
        frame.add(numberOfQueues, constraints);
        constraints.gridy = 6;
        frame.add(simTime, constraints);
        constraints.gridx = 3;
        constraints.gridy = 0;
        constraints.gridheight = 5;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        frame.add(new JScrollPane(textArea), constraints);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(480, 320);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        buttonStart.addActionListener( new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
//            	shop.noQueues = Integer.parseInt(numberOfQueues.getText());
//            	shop.simulationDuration = Long.parseLong(simTime.getText())*1000;
//            	shop.minServiceTime = Integer.parseInt(minimumServiceTime.getText())*1000;
//            	shop.maxServiceTime = Integer.parseInt(maximumServiceTime.getText())*1000;
//            	shop.minInterval = Integer.parseInt(minimumArrivingTime.getText())*1000;
//            	shop.maxInterval = Integer.parseInt(maximumArrivingTime.getText())*1000;
//            	shop.start();
            }
        });
	}

}
